from wagtail.images.formats import Format, register_image_format

register_image_format(Format('full-centered-image', 'Full centered image', 'mw-100 h-auto my-2', 'original'))