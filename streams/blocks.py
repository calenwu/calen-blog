"""Streamfields live in here."""

from wagtail.core import blocks
from wagtail.images.blocks import ImageChooserBlock

class RichtextBlock(blocks.RichTextBlock):
	"""Richtext with all features."""

	class Meta: #noga
		template = 'streams/richtext_block.html'
		icon = 'doc-full'
		label = 'Richtext'


class HtmlBlock(blocks.RawHTMLBlock):
	"""Html block."""

	class Meta: #noga
		template = 'streams/raw_html_block.html'
		icon = 'code'
		label = 'Html'


class CodeBlock(blocks.StructBlock):
	"""Html block."""
	language = blocks.CharBlock(required=True, help_text='Programming language')
	code = blocks.TextBlock(repuired=True)

	class Meta: #noga
		template = 'streams/code_block.html'
		icon = 'code'
		label = 'Code'


class CardBlock(blocks.StructBlock):
	title = blocks.CharBlock(required=True, help_text='Title')
	cards = blocks.ListBlock(
		blocks.StructBlock(
			[
				('image', ImageChooserBlock(required=True)),
				('title', blocks.CharBlock(required=True, max_length=63)),
				('text', blocks.CharBlock(required=True, max_length=255)),
				('button_page', blocks.PageChooserBlock(required=False)),
				('button_url', blocks.URLBlock(required=False, help_text='Link')),
				('button_text', blocks.CharBlock(required=True, max_length=63)),
			]
		)
	)

	class Meta: #noga
		template = 'streams/card_block.html'
		icon = 'placeholder'
		label = 'Card'

