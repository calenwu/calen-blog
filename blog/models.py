"""Blog listing and blog detail pages"""
from django import forms
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.db import models
from django.shortcuts import render

from django_extensions.db.fields import AutoSlugField

from modelcluster.models import ClusterableModel, ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
#in line 44, add {} to _to_tag_model_instances = tag_objs = self._to_tag_model_instances(tags, {})

from taggit.models import TaggedItemBase

from wagtail.admin.edit_handlers import FieldPanel, PageChooserPanel, StreamFieldPanel, InlinePanel, MultiFieldPanel
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.core.fields import StreamField
from wagtail.core.models import Page, Orderable
from wagtail.images.edit_handlers import ImageChooserPanel
from wagtail.snippets.edit_handlers import SnippetChooserPanel
from wagtail.snippets.models import register_snippet

from streams import blocks

from account.models import User


class ListingPage(RoutablePageMixin, Page):
	"""Listing page lists all the Blog Detail Pages."""
	max_count = 1
	template = 'blog/listing.html'
	subpage_types = ['ArticlePage']

	def get_admin_display_title(self):
		return 'Blog Listing Page'

	def __str__(self):
		return 'Blog Listing Page'

	def get_context(self, request, *args, **kwargs):
		context = super().get_context(request, *args, **kwargs)
		context['listing_url'] = self.url
		context['page'] = request.GET.get('page', None)
		context['tags'] = request.GET.get('tags', None)
		context['search'] = request.GET.get('search', None)
		searches = None
		if context['search'] and context['search'] != 'None':
			context['searches'] = context['search'].split(';;;')[:-1]
			context['search'].replace(';;;', ',')
		return context


class Tag(TaggedItemBase):
	content_object = ParentalKey(
		'ArticlePage',
		related_name='blog_tags',
		on_delete=models.CASCADE
	) #cant change name


class AuthorsOrderable(Orderable):
	""""""
	page = ParentalKey('ArticlePage', related_name='blog_authors')
	author = models.ForeignKey(User, on_delete=models.CASCADE)
	unique_together = ('page', 'author')
	panels = [SnippetChooserPanel('author'),]


class Category(ClusterableModel):
	"""Category for a snippet."""
	name = models.CharField(max_length=255)
	slug = AutoSlugField(populate_from='name', editable=True)

	panels = [
		FieldPanel('name'),
		FieldPanel('slug'),
	]

	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Blog Categories'
		ordering = ['name']

	def __str__(self):
		return self.name
register_snippet(Category)


class CategoriesOrderable(Orderable, Category):
	""""""
	page = ParentalKey('ArticlePage', related_name='blog_categories')


class ArticlePage(RoutablePageMixin, Page):
	"""Blog Artice"""
	subpage_types = []
	parent_page_types = [ListingPage]
	tags = ClusterTaggableManager(through=Tag, blank=True)
	template = 'blog/article.html'
	subtitle = models.CharField(max_length=255, blank=True, null=True, help_text='Why we love Avicii')
	authors = ParentalManyToManyField(User, blank=False, related_name='blog_authors')
	categories = ParentalManyToManyField(Category, blank=True, null=True, related_name='blog_categories')
	preview_image = models.ForeignKey(
		'wagtailimages.Image',
		blank=True,
		null=True,
		related_name='+',
		on_delete=models.SET_NULL
	)
	preview_text = models.TextField(blank=True, null=True)
	content = StreamField(
		[
			('richtext', blocks.RichtextBlock()),
			('code', blocks.CodeBlock()),
			('html', blocks.HtmlBlock())
		], 
		null=True,
		blank=True
	)
	content_panels = Page.content_panels + [
		FieldPanel('subtitle'),
		ImageChooserPanel('preview_image'),
		FieldPanel('preview_text'),
		MultiFieldPanel(
			[
				#FieldPanel('authors', widget=forms.CheckboxSelectMultiple)
				FieldPanel('authors', widget=forms.CheckboxSelectMultiple)
			], heading='Author(s)'
		),
		MultiFieldPanel(
			[
				InlinePanel('blog_categories')
			], heading='Categories'
		),
		FieldPanel('tags'),
		StreamFieldPanel('content')
	]

	def save(self, *args, **kwargs):
		key = make_template_fragment_key('article', [self.id])
		cache.delete(key)
		return super().save(*args, **kwargs)


"""
class Category(models.Model):
	"Category for a snippet.""
	name = models.CharField(max_length=255)
	slug = AutoSlugField(populate_from='name', editable=True)

	panels = [
		FieldPanel('name'),
		FieldPanel('slug'),
	]

	class Meta:
		verbose_name = 'Category'
		verbose_name_plural = 'Blog Categories'
		ordering = ['name']

	def __str__(self):
		return self.name
register_snippet(Category)
"""




