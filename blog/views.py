from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.db.models import Q
from django.shortcuts import render, get_object_or_404
from django.views.generic import ListView

from functools import reduce

from account.models import User
from .forms import ArticlesFilterForm
from .models import ListingPage, ArticlePage

import operator

def get_paginator_items(paginator, page):
	"""
	Get paginatior items
	"""
	try:
		items = paginator.page(page)
	except PageNotAnInteger:
		items = paginator.page(1)
	except EmptyPage:
		items = paginator.page(paginator.num_pages)
	return items


def get_page_from_request(request):
	return request.GET.get('page')


def artiles_content(request):
	"""
	Articles list content view
	"""
	searches = None
	search = request.GET.get('search', None)
	articles = ArticlePage.objects.live().public().order_by('-first_published_at')
	if search:
		empty = False
		searches = search.split(';;;')[:-1]
		all_authors = User.objects.all()
		for search_block in searches:
			search_blocks = search_block.split(': ', 1)
			if search_blocks[0] == 'article':
				articles = articles.filter(title=search_blocks[1])
			elif search_blocks[0] == 'tag':
				articles = articles.filter(tags__name__in=[search_blocks[1]])
			elif search_blocks[0] == 'author':
				found = False
				for author in all_authors:
					if str(author) == search_blocks[1]:
						articles = articles.filter(authors__pk=author.pk)
						found = True
				if not found:
					articles = []
			elif search_blocks[0] == 'category':
				articles = articles.filter(categories__name__contains=search_blocks[1])
			else:
				articles = articles.filter(title__contains=search_blocks[0])
			if not articles:
				articles = ArticlePage.objects.filter(title='ajdsfljlksjdfklioewwmncgdfkeiwk')
				break
	page = get_page_from_request(request)
	return render(request, 'blog/listing_content.html', {
		'articles': get_paginator_items(Paginator(articles, 12), page),
		'page': page,
		'searches': searches,
		'listing_url': ListingPage.objects.all().first().url
	})


def handler500(request):
	return render(request, '500.html', status=500)
