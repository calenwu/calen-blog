from .models import Category, ArticlePage

from graphene_django import DjangoObjectType
from taggit.models import Tag

import graphene

class Query(graphene.ObjectType):
	tags = graphene.List(graphene.String)
	categories = graphene.List(graphene.String)
	articles = graphene.List(graphene.String)

	def resolve_tags(self, info):
		return list(Tag.objects.all().values_list('name', flat=True))

	def resolve_categories(self, info):
		return list(Category.objects.all().values_list('name', flat=True))

	def resolve_articles(self, info):
		return list(ArticlePage.objects.live().values_list('title', flat=True))

