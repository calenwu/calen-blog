from django import template


register = template.Library()

@register.simple_tag
def page_min(page=None):
  try:
    if page < 4:
      return 1
    return int(page) - 2
  except Exception:
    pass
  return 1


@register.simple_tag
def page_max(page=None):
  try:
    return int(page) + 2
  except Exception:
    pass
  return 1