from django.db import models

from wagtail.admin.edit_handlers import (
	StreamFieldPanel
)
from wagtail.core.fields import StreamField
from wagtail.core.models import Page

from streams import blocks


class HomePage(Page):
	max_count = 1
	templates = 'home/home_page.html'
	parent_page_type = [
		'wagtailcore.Page'
	]
	content = StreamField(
		[
			('richtext', blocks.RichtextBlock()),
			('html_block', blocks.HtmlBlock())
		],
		null=True,
		blank=True
	)
	content_panels = Page.content_panels + [
		StreamFieldPanel('content')
	]