//document.addEventListener("DOMContentLoaded", function() {
//    var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
//    if ("IntersectionObserver" in window) {
//        let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
//            entries.forEach(function(entry) {
//                if (entry.isIntersecting) {
//                    let lazyImage = entry.target;
//                    lazyImage.src = lazyImage.dataset.src;
//                    lazyImage.classList.remove("lazy");
//                    lazyImageObserver.unobserve(lazyImage);
//                }
//            });
//        });
//        lazyImages.forEach(function(lazyImage) {
//            lazyImageObserver.observe(lazyImage);
//        });
//    } else {
//        // Possibly fall back to a more compatible method here
//    }
//});

function transformButtonToLoad(btn, spinnerColor) {
  btn.innerText = '';
  btn.appendChild(getSpinnerDots(spinnerColor));
  btn.disabled = true;
}

function transformButtonToNormal(btn, text) {
  removeAllChilds(btn);
  btn.disabled = false;
  btn.innerText = text;
}

function setAttributes(el, attrs) {
  for (let key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
  return el
}

function dontTriggerBelow() {
  if (!e) var e = window.event;
  e.cancelBubble = true;
  if (e.stopPropagation) e.stopPropagation();
}

function getDate() {
  let today = new Date();
  let dd = today.getDate();
  let mm = today.getMonth() + 1; //January is 0!
  let yyyy = today.getFullYear();
  if (dd < 10) {
    dd = '0' + dd;
  }
  if (mm < 10) {
    mm = '0' + mm;
  }
  return mm + '/' + dd + '/' + yyyy;
}

function changeUrl(url) {
  window.location = url;
}

function removeAllChilds(myNode) {
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }
}

function createAlert(tag, message, timeout) {
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'alert alert-dismissible fade show alert-' + tag,
  });
  let span = setAttributes(document.createElement('span'), {
    'class': 'close cursor-pointer',
    'data-dismiss': 'alert',
    'aria-label': 'close',
  });
  let closeIcon = setAttributes(document.createElement('i'), {
    'class': 'icon-cancel',
  });
  firstDiv.innerText = message;
  span.appendChild(closeIcon);
  firstDiv.appendChild(span);
  let snackbar = document.getElementById('snackbar');
  snackbar.appendChild(firstDiv);
  if (timeout) {
    setTimeout(function () {
      span.click();
    }, timeout);
  } else {
    setTimeout(function () {
      span.click();
    }, 4000);
  }
}
fancyAlertCounter = 0;
function createFancyAlert(tag, title, innerHTML, timeout) {
  let firstDiv = setAttributes(document.createElement('div'), {
    'id': 'fancy-alert-' + fancyAlertCounter.toString(),
    'class': 'd-flex justify-content-between bg-white p-3 fancy-alert-container fancy-alert-container-' + tag,
  });
  let secondDiv = setAttributes(document.createElement('div'), {
    'class': 'd-flex',
  });
  let thirdDiv = setAttributes(document.createElement('div'), {
    'class': '',
  });
  let icon = "";
  if (tag == 'success') {
    icon = setAttributes(document.createElement('i'), {
      'class': 'fancy-alert-icon fancy-alert-color-' + tag + ' icon-ok-circled2',
    });
  } else {
    icon = setAttributes(document.createElement('i'), {
      'class': 'fancy-alert-icon fancy-alert-color-' + tag + ' icon-cancel-circled2',
    });
  }
  thirdDiv.appendChild(icon);
  let fourthDiv = setAttributes(document.createElement('i'), {
    'class': 'pl-2',
  });
  let titleDiv = setAttributes(document.createElement('div'), {
    'class': 'text-left font-weight-bold mb-1 fancy-alert-title-' + tag
  });
  let text = setAttributes(document.createElement('div'), {
    'class': 'font-small mb-1',
  });
  titleDiv.innerText = title;
  text.innerHTML = innerHTML;
  fourthDiv.appendChild(titleDiv);
  fourthDiv.appendChild(text);
  secondDiv.appendChild(thirdDiv);
  secondDiv.appendChild(fourthDiv);
  let closeContainer = setAttributes(document.createElement('div'), {
    'class': 'd-flex justify-content-center align-items-center ml-2',
  });
  let closeIconContainer = setAttributes(document.createElement('span'), {
    'class': 'p-1 fancy-alert-color-' + tag,
    'onClick': 'closeFancyAlert(' + fancyAlertCounter.toString() + ')'
  });
  let closeIcon = setAttributes(document.createElement('i'), {
    'class': 'fancy-alert-close icon-cancel cursor-pointer',
  });
  closeIconContainer.appendChild(closeIcon);
  closeContainer.appendChild(closeIconContainer);
  firstDiv.appendChild(secondDiv);
  firstDiv.appendChild(closeContainer);
  let snackbar = document.getElementById('snackbar');
  snackbar.appendChild(firstDiv);
  setTimeout(function () {
    firstDiv.classList.add('opacity-1');
  }, timeout);
  fancyAlertCounter = fancyAlertCounter + 1;
  if (timeout) {
    setTimeout(function () {
      closeIconContainer.click();
    }, timeout);
  } else {
    setTimeout(function () {
      closeIconContainer.click();
    }, 5000);
  }
}

function closeFancyAlert(alert_id) {
  let alert = document.getElementById('fancy-alert-' + alert_id.toString());
  alert.classList.remove('opacity-1');
  setTimeout(function () {
    alert.style.height = 0;
  }, 250);
  setTimeout(function () {
    removeElement('fancy-alert-' + alert_id.toString());
  }, 500);
}

function showSnackbar() {
  var snackbar = document.getElementById("snackbar");
  snackbar.className = "show";
  setTimeout(function () {
    snackbar.className = snackbar.className.replace("show", "");
  }, 5000);
}

function getSpinner() {
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-border',
    'role': 'status'
  });
  let span = setAttributes(document.createElement('span'), {
    'class': 'sr-only',
  });
  span.innerText = spinnerLoadingText;
  firstDiv.appendChild(span);
  return firstDiv
}

function getSpinnerDotsContainer(color) {
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots-container',
  });
  firstDiv.appendChild(getSpinnerDots(color));
  return firstDiv
}

function getSpinnerDots(color) {
  let megaDiv = setAttributes(document.createElement('div'), {
    'class': 'd-flex w-100 justify-content-center align-items-center',
  });
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots spinner-dots-' + color,
  });
  let bounce1 = setAttributes(document.createElement('div'), {
    'class': 'bounce1',
  });
  let bounce2 = setAttributes(document.createElement('div'), {
    'class': 'bounce2',
  });
  let bounce3 = setAttributes(document.createElement('div'), {
    'class': 'bounce3',
  });
  let invis1 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  let invis2 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  invis1.innerHTML = 'h';
  invis2.innerHTML = 'h';
  firstDiv.appendChild(bounce1);
  firstDiv.appendChild(bounce2);
  firstDiv.appendChild(bounce3);
  megaDiv.appendChild(invis1);
  megaDiv.appendChild(firstDiv);
  megaDiv.appendChild(invis2);
  return megaDiv
}

function getSpinnerDotsContainerMedium(color) {
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots-container',
  });
  firstDiv.appendChild(getSpinnerDotsMedium(color));
  return firstDiv
}

function getSpinnerDotsMedium(color) {
  let megaDiv = setAttributes(document.createElement('div'), {
    'class': 'd-flex w-100 justify-content-center align-items-center',
  });
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots spinner-dots-medium spinner-dots-' + color,
  });
  let bounce1 = setAttributes(document.createElement('div'), {
    'class': 'bounce1',
  });
  let bounce2 = setAttributes(document.createElement('div'), {
    'class': 'bounce2',
  });
  let bounce3 = setAttributes(document.createElement('div'), {
    'class': 'bounce3',
  });
  let invis1 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  let invis2 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  invis1.innerHTML = 'h';
  invis2.innerHTML = 'h';
  firstDiv.appendChild(bounce1);
  firstDiv.appendChild(bounce2);
  firstDiv.appendChild(bounce3);
  megaDiv.appendChild(invis1);
  megaDiv.appendChild(firstDiv);
  megaDiv.appendChild(invis2);
  return firstDiv
}

function getSpinnerDotsContainerBig(color) {
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots-container spinner-dots-container-big',
  });
  firstDiv.appendChild(getSpinnerDotsBig(color));
  return firstDiv
}

function getSpinnerDotsBig(color) {
  let megaDiv = setAttributes(document.createElement('div'), {
    'class': 'd-flex w-100 justify-content-center align-items-center',
  });
  let firstDiv = setAttributes(document.createElement('div'), {
    'class': 'spinner-dots spinner-dots-big spinner-dots-' + color,
  });
  let bounce1 = setAttributes(document.createElement('div'), {
    'class': 'bounce1',
  });
  let bounce2 = setAttributes(document.createElement('div'), {
    'class': 'bounce2',
  });
  let bounce3 = setAttributes(document.createElement('div'), {
    'class': 'bounce3',
  });
  let invis1 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  let invis2 = setAttributes(document.createElement('div'), {
    'class': 'invisible',
  });
  invis1.innerHTML = 'h';
  invis2.innerHTML = 'h';
  firstDiv.appendChild(bounce1);
  firstDiv.appendChild(bounce2);
  firstDiv.appendChild(bounce3);
  megaDiv.appendChild(invis1);
  megaDiv.appendChild(firstDiv);
  megaDiv.appendChild(invis2);
  return firstDiv
}

function setProgressBar(element, percent) {
  element.setAttribute('aria-valuenow', percent);
  element.style.width = percent + '%';
}

function removeElement(id) {
  document.getElementById(id).outerHTML = "";
}

function removeCheckboxElement(id, id_checkbox) {
  document.getElementById(id).style.display = "none";
  document.getElementById(id_checkbox).checked = true;
}

function elementToInnerHTML(element) {
  var wrap = document.createElement('div');
  wrap.appendChild(element.cloneNode(true));
  return wrap.innerHTML;
}

function removeChildren(node) {
  while (node.firstChild) {
    node.removeChild(node.firstChild);
  }
}

function scrollToElement(element) {
  element.scrollIntoView();
  window.scrollBy(0, -100);
}

function disableButton(element, color) {
  removeAllChilds(element)
  element.appendChild(getSpinnerDots(color));
  element.disabled = true;
}

function enableButton(element, text) {
  element.disabled = false;
  element.innerText = text
}

function showOverlay(overlay_id) {
  let overlay = document.getElementById(overlay_id);
  overlay.classList.add('overlay-active');
  overlay.classList.add('z-index-2000');
}

function hideOverlay(overlay_id) {
  let overlay = document.getElementById(overlay_id);
  overlay.classList.remove('overlay-active');
  setTimeout(function () {
    overlay.classList.remove('z-index-2000');
  }, 500);
}

async function loadContent(container_id, url, color) {
  let container = document.getElementById(container_id);
  const oldChildren = container.children;
  container.classList.add('pt-5');
  removeAllChilds(container);
  let spinnerDots = getSpinnerDotsContainerMedium(color);
  container.classList.add('min-height-240', 'd-flex', 'justify-content-center', 'align-items-center');
  container.appendChild(spinnerDots);
  const startTime = Date.now();
  await fetch(url)
    .then(response => {
      return response.text()
    })
    .then(html => {
      const elapsedTime = ((Date.now() - startTime));
      if (elapsedTime > 400){
        removeAllChilds(container);
        container.classList.remove('min-height-240', 'd-flex', 'justify-content-center', 'align-items-center');
        container.appendChild(new DOMParser().parseFromString(html, 'text/html').querySelector('div'));
      } else {
        setTimeout(() => {
          removeAllChilds(container);
          container.appendChild(new DOMParser().parseFromString(html, 'text/html').querySelector('div'));
        },  400 - elapsedTime);
      }
    })
    .catch(error => {
      removeAllChilds(container);
      for(let i = 0; i < oldChildren.length; i++){
        container.appendChild(oldChildren[i]);
      }
      createFancyAlert('error', 'Loading error', 'There was an error trying to load the content');
      throw error;
    })
  container.classList.remove('pt-5');
}

async function loadContentSkeleton(container_id, url, skeleton) {
  let container = document.getElementById(container_id);
  const oldChildren = container.children;
  removeAllChilds(container);
  container.appendChild(skeleton);
  const startTime = Date.now();
  await fetch(url)
    .then(response => {
      return response.text()
    })
    .then(html => {
      const elapsedTime = ((Date.now() - startTime));
      if (elapsedTime > 400){
        removeAllChilds(container);
        container.appendChild(new DOMParser().parseFromString(html, 'text/html').querySelector('div'));
      } else {
        setTimeout(() => {
          removeAllChilds(container);
          container.appendChild(new DOMParser().parseFromString(html, 'text/html').querySelector('div'));
        },  400 - elapsedTime);
      }
    })
    .catch(error => {
      removeAllChilds(container);
      for(let i = 0; i < oldChildren.length; i++){
        container.appendChild(oldChildren[i]);
      }
      createFancyAlert('error', 'Loading error', 'There was an error trying to load the content');
      throw error;
    })
  return true;
}
async function postData(url, data) {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json'
    },
    redirect: 'follow',
    referrer: 'no-referrer',
    body: JSON.stringify(data)
  });
  return await response.json();
}