"""Menu models"""
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.core.exceptions import ValidationError 
from django.db import models
from django_extensions.db.fields import AutoSlugField

from modelcluster.models import ClusterableModel, ParentalKey
from wagtail.core.models import Orderable
from wagtail.admin.edit_handlers import (
	FieldPanel,
	InlinePanel,
	MultiFieldPanel,
	PageChooserPanel
)
from wagtail.snippets.models import register_snippet
# Create your models here.


class MenuItem(Orderable):
	title = models.CharField(max_length=127)
	slug = AutoSlugField(populate_from='title', editable=True, blank=False, null=False)
	link_page = models.ForeignKey(
		'wagtailcore.Page',
		null=True,
		blank=True,
		on_delete=models.CASCADE,
		related_name='+'
	)
	link_url = models.CharField(max_length=255, blank=True, null=True)
	open_in_new_tab = models.BooleanField(default=False)
	page = ParentalKey('menu', related_name='menu_item')
	panels = [
		FieldPanel('title'),
		FieldPanel('slug'),
		PageChooserPanel('link_page'),
		FieldPanel('link_url'),
		FieldPanel('open_in_new_tab'),
	]

	@property
	def link(self) -> str:
		if self.link_page:
			return self.link_page.url
		else:
			return self.link_url

	def save(self, *args, **kwargs):
		key = make_template_fragment_key('navigation')
		cache.delete(key)
		return super().save(*args, **kwargs)

	def clean(self):
		if self.link_page is None and self.link_url is None:
			raise ValidationError('Either link_page or link_url has to be set') 


@register_snippet
class Menu(ClusterableModel):
	"""The main menu clusterable model."""
	title = models.CharField(max_length=127)
	slug = AutoSlugField(populate_from='title', editable=True)
	panels = [
		MultiFieldPanel([
			FieldPanel('title'),
			FieldPanel('slug'),
		], heading='Menu'),
		InlinePanel('menu_item', label='Menu Item')
	]

	def __str__(self) -> str:
		return self.title
