from .models import User

import graphene


class Query(graphene.ObjectType):
	authors = graphene.List(graphene.String)

	def resolve_authors(self, info):
		return [str(x) for x in User.objects.all()]

