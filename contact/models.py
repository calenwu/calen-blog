from django.contrib import messages
from django.db import models
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _

from modelcluster.fields import ParentalKey
from wagtail.admin.edit_handlers import (
	FieldPanel,
	FieldRowPanel,
	InlinePanel,
	MultiFieldPanel
)
from wagtail.contrib.forms.edit_handlers import FormSubmissionsPanel
from wagtail.core.fields import RichTextField
from wagtail.contrib.forms.models import (
	AbstractEmailForm,
	AbstractFormField
)
from wagtailcaptcha.models import WagtailCaptchaEmailForm


class FormField(AbstractFormField):
	page = ParentalKey(
		'ContactPage',
		on_delete=models.CASCADE,
		related_name='form_fields',
	)


class ContactPage(WagtailCaptchaEmailForm):
	template = 'contact/contact_page.html'
	landing_page_template = "contact/contact_page.html"
	subpage_types = []
	parent_page_types = ['home.HomePage']
	submit_button_text = models.CharField(max_length=63)
	content_panels = AbstractEmailForm.content_panels + [
		FormSubmissionsPanel(),
		InlinePanel('form_fields', label='Form Fields'),
		FieldPanel('submit_button_text'),
		MultiFieldPanel([
			FieldRowPanel([
				FieldPanel('from_address', classname='col6'),
				FieldPanel('to_address', classname='col6'),
			]),
			FieldPanel('subject'),
		], heading='Email Settings'),
	]

	def get_context(self, request, *args, **kwargs):
		form = self.get_form()
		return super().get_context(request, *args, **kwargs)

	def render_landing_page(self, request, form_submission=None, *args, **kwargs):
		url = '/contact'
		# if a form_submission instance is available, append the id to URL
		# when previewing landing page, there will not be a form_submission instance
		messages.success(request, 'Message sent')
		if form_submission:
			url += '?id=%s' % form_submission.id
			return redirect(url, permanent=False)
		# if no thank_you_page is set, render default landing page
		return super().render_landing_page(request, form_submission, *args, **kwargs)
