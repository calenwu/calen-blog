from django.db import models
from django.core.validators import MinValueValidator
from django.utils import timezone

from modelcluster.models import ClusterableModel, ParentalKey, ParentalManyToManyField

from wagtail.core.models import Page
from wagtail.contrib.routable_page.models import RoutablePageMixin, route
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.snippets.models import register_snippet
# Create your models here.

class PortfolioPage(RoutablePageMixin, Page):
	"""Portfolio Page"""
	max_count = 1
	template = 'portfolio/portfolio.html'

	def get_admin_display_title(self):
		return 'Portfolio Page'

	def __str__(self):
		return 'Portfolio Page'

	def get_context(self, request, *args, **kwargs):
		context = super().get_context(request, *args, **kwargs)
		context['assets'] = Asset.objects.filter(public=True)
		return context


class Asset(ClusterableModel):
	ACTION_CHOICES = (
    ('long','Long'),
    ('short', 'Short'),
	)
	"""Category for a snippet."""
	ticker = models.CharField(max_length=15)
	name = models.CharField(max_length=255, blank=True, null=True)
	price = models.FloatField(max_length=255)
	quantity = models.IntegerField(validators=[MinValueValidator(1)])
	currency = models.CharField(max_length=7)
	action = models.CharField(max_length=7, choices=ACTION_CHOICES, default='long')
	date_time = models.DateTimeField(default=timezone.now)
	entry = models.ForeignKey('portfolio.Asset', on_delete=models.SET_NULL, blank=True, null=True)
	public = models.BooleanField(default=True)

	panels = [
		FieldPanel('ticker'),
		FieldPanel('name'),
		FieldPanel('price'),
		FieldPanel('quantity'),
		FieldPanel('currency'),
		FieldPanel('action'),
		FieldPanel('date_time'),
		FieldPanel('entry'),
		FieldPanel('public'),
	]

	class Meta:
		verbose_name = 'Asset'
		verbose_name_plural = 'Assets'
		ordering = ['date_time']

	def __str__(self):
		return self.ticker + ' ' + str(self.date_time)

	def total(self):
		return self.price * self.quantity

	def get_entry(self):
		return 'Entry' if self.entry is None else self.entry.id

	def get_win_loss(self):
		if self.entry is None:
			return ''
		if self.action == 'long':
			return (self.price - self.entry.price) * self.quantity
		return (self.entry.price - self.price) * self.quantity

	def get_date(self):
		return self.date_time.strftime("%d/%m/%Y, %H:%M:%S")


register_snippet(Asset)