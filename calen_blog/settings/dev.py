from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '8fuf5xmbcecct-r+fok(!#fdb94b$%0!1qdat^5!dez@@-(g#1'

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ['*']

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

INSTALLED_APPS += [
	'debug_toolbar',
	'django_extensions',
	'wagtail.contrib.styleguide'
]

MIDDLEWARE += [
	'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = ('127.0.0.1',)

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql',
		'NAME': 'calen_blog',
		'USER': 'postgres',
		'PASSWORD': 'postgres',
		'HOST': 'postgres',
		'PORT': '5432',
	}
}

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.dummy.DummyCache',
	}
}
