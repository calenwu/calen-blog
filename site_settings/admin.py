from wagtail.contrib.modeladmin.options import (
	ModelAdmin,
	modeladmin_register
)
from .models import SocialMedia
# Register your models here.

class SocialMediaAdmin(ModelAdmin):
	"""Social Media admin."""
	model = SocialMedia
	menu_label = 'Social Media'
	menu_icon = 'group'
	menu_order = 290
	add_to_settings_menu = False
	exclude_from_explorer = False
	list_display = ('name', 'url', 'icon_classname')
	search_fields = ('name', 'url')

modeladmin_register(SocialMediaAdmin)