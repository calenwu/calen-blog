from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key
from django.db import models

# Create your models here.
class SocialMedia(models.Model):
	"""A social media mobel."""
	name = models.CharField(blank=False, null=False, help_text="Instagram", max_length=63)
	url = models.CharField(blank=False, null=False, help_text="https://instagram.com/username", max_length=255)
	icon_classname = models.CharField(blank=False, null=False, help_text="icon-instagram", max_length=64)

	def __str__(self):
		"""Str repr of this object"""
		return self.name

	class Meta:
		verbose_name = 'Social Media'
		verbose_name_plural = 'Social Media'

	def save(self, *args, **kwargs):
		key = make_template_fragment_key('social_media')
		cache.delete(key)
		return super().save(*args, **kwargs)
