from django import template
from ..models import SocialMedia


register = template.Library()
@register.simple_tag()
def get_social_media():
	return SocialMedia.objects.all()
